# locker-service
GS locker-service microservice ...

# Commands
- sam init --location git@bitbucket.org:goldenscent/stencil.git
- pipenv shell
- pipenv install --dev
- make test
- invoke both functions
make invoke fn=ApiHelloGet ef=api-hello-get.json
make invoke fn=ApiHelloPost ef=api-hello-post.json
- start api
- make bootsrap
- make deploy stage=dev

# Tools we use
- pipenv
- SAM
- Swagger
- API Gateway
- Lambda Function
- pytest
- python
- pydantic
- typing

# Redbox Integration Basic Flow - Customer Facing
![picture](assets/Redbox1.png)

# Reads
- https://github.com/awslabs/aws-lambda-powertools-python
- https://realpython.com/pipenv-guide/
- https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html