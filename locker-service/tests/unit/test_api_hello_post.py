from functions import api_hello_post as hello_post
from functions.models import Hello
import json

def test_api_hello_get_handler(api_hello_post_event, lambda_context):
    response = hello_post.handler(api_hello_post_event, lambda_context)
    body = response.get('body')
    hello = Hello(**json.loads(body))
    assert hello.first_name == 'mam'