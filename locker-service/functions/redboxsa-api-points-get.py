from aws_lambda_powertools.utilities.parser import ValidationError
from redboxsa_models import RedboxsaPointsRequest, RedboxsaPointsResponse
import redboxsa_helper
import json
import sys
import os


def handler(e, _):
    try:
        request_body = RedboxsaPointsRequest.parse_obj(
            e['pathParameters']
        )

        response = redboxsa_helper.getPoints(request_body.lat, request_body.lng, request_body.distance)

        if response.status_code != 200:
            status_code = response.status_code
            body = {"message": response.text}
        else:
            body = RedboxsaPointsResponse.parse_obj(
                json.loads(response.text)
            ).dict()
            status_code = response.status_code

    except ValidationError as error:
        status_code = 403
        body = {"message": error.errors()}
    except Exception as error:
        status_code = 403
        body = {"message": error}

    return {
        "statusCode": status_code,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        "body": json.dumps(body),
        "isBase64Encoded": False
    }
