from typing import Dict, List, Optional
from pydantic import BaseModel


class Event(BaseModel):
    resource: str
    body: Optional[str]
    queryStringParameters: Optional[Dict[str, str]]
    multiValueQueryStringParameters: Optional[Dict[str, List]]


class Hello(BaseModel):
    first_name: str
    last_name: Optional[str]
