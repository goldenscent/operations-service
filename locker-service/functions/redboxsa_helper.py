import os
import requests
import boto3
import sys


def getPoints(lat, lng, distance):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "GET",
        f"{os.environ['REDBOXSA_BASE_URL']}get-points?lat={lat}&lng={lng}&distance={distance}",
        headers=headers,
    )
    return response


def getPointDetails(point_id):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "GET",
        f"{os.environ['REDBOXSA_BASE_URL']}get-point-detail?point_id={point_id}",
        headers=headers,
    )
    return response


def postCreateShipment(payload):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "POST",
        f"{os.environ['REDBOXSA_BASE_URL']}create-shipment",
        headers=headers,
        data=payload
    )
    return response


def putUpdateShipment(payload):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "PUT",
        f"{os.environ['REDBOXSA_BASE_URL']}update-shipment",
        headers=headers,
        data=payload
    )
    return response


def getListShipments(page):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "GET",
        f"{os.environ['REDBOXSA_BASE_URL']}get-shipments?page={page}",
        headers=headers,
    )
    return response

def getShipmentDetail(tracking_number):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "GET",
        f"{os.environ['REDBOXSA_BASE_URL']}shipment-detail?tracking_number={tracking_number}",
        headers=headers,
    )
    return response


def getShipmentShippingLabel(tracking_number):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "GET",
        f"{os.environ['REDBOXSA_BASE_URL']}get-shipment-shipping-label?tracking_number={tracking_number}",
        headers=headers,
    )
    return response


def putCancelShipment(payload):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "PUT",
        f"{os.environ['REDBOXSA_BASE_URL']}cancel-shipment",
        headers=headers,
        data=payload
    )
    return response


def postReturnShipment(payload):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "POST",
        f"{os.environ['REDBOXSA_BASE_URL']}return-shipment",
        headers=headers,
        data=payload
    )
    return response


def postUpdateShipmentCod(payload):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "POST",
        f"{os.environ['REDBOXSA_BASE_URL']}update-cod-shipment",
        headers=headers,
        data=payload
    )
    return response


def postExtendShipment(payload):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "POST",
        f"{os.environ['REDBOXSA_BASE_URL']}extend-shipment",
        headers=headers,
        data=payload
    )
    return response


def postCreateRequestPickup(payload):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "POST",
        f"{os.environ['REDBOXSA_BASE_URL']}create-request-pick-up",
        headers=headers,
        data=payload
    )
    return response

def getShipmentStatus(tracking_number):
    headers = {
        'Authorization': "Bearer " + os.environ['REDBOXSA_API_KEY'],
        'Content-Type': 'application/json',
        'charset': 'utf - 8'
    }
    response = requests.request(
        "GET",
        f"{os.environ['REDBOXSA_BASE_URL']}get-shipment-status?tracking_number={tracking_number}",
        headers=headers,
    )
    return response