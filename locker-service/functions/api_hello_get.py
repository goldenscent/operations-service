from models import Event
import helper
import json
import random

helper = helper.Helper()


def handler(e, _):
    try:
        event = Event(**e)
        id = int(event.multiValueQueryStringParameters['id'][0])
        body = helper.get_hello(id)
        return {
            "statusCode": 200,
            "headers": {
                'Content-Type': 'application/json',
                'Random-Num': random.randint(0,1000),
            },
            "body": json.dumps(body.dict()),
            "isBase64Encoded": False
        }
    except Exception as error:
        raise
