from models import Hello


class Helper:
    f_names = [
        'Bubna',
        'mAm',
        'Nebil',
        'Yazan',
        'Ahmed'
    ]
    def get_hello(self, id):
        return Hello(**{
            'first_name': self.f_names[id-1]
        })

    def save_hello(self, request_body: Hello):
        # Saving ....
        return Hello(**{
            'first_name': request_body.first_name,
            'last_name' : request_body.last_name
        })
