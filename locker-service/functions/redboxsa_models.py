from typing import Dict, List, Optional, Literal
from pydantic import BaseModel, root_validator, parse_obj_as, ValidationError, validator
from datetime import datetime
from enum import Enum, IntEnum


class Event(BaseModel):
    resource: str
    body: Optional[Dict]
    queryStringParameters: Optional[Dict[str, str]]
    multiValueQueryStringParameters: Optional[Dict[str, List]]


# ######### points request #########
class RedboxsaPointsRequest(BaseModel):
    lat: Optional[float]
    lng: Optional[float]
    city: Optional[str]
    district: Optional[str]
    distance: str


class RedboxsaPointsResponse(BaseModel):
    success: bool
    points: List


# ######### points request #########

# ######### points details #########
class RedboxsaPointDetailsRequest(BaseModel):
    point_id: str


class RedboxsaPointDetailsResponse(BaseModel):
    success: bool
    point: Dict


# ######### points details #########


# ######### create shipment #########
class RedboxsaCreateShipmentRequest(BaseModel):
    items: List
    reference: str
    customer_name: str
    customer_phone: str
    customer_email: str
    customer_address: str
    cod_currency: str
    cod_amount: str
    size: str  # Required and one of sizes: "Small", "Medium", "Large"
    tracking_number: Optional[str]
    business_id: Optional[str]
    point_id: Optional[str]
    dimension_unit: Optional[str]
    dimesion_length: Optional[float]
    dimension_width: Optional[float]
    dimension_height: Optional[float]
    weight_unit: Optional[str]
    weight_value: Optional[float]


class RedboxsaCreateShipmentResponse(BaseModel):
    success: bool
    shipment_id: str
    tracking_number: str
    url_shipping_label: str


# ######### create shipment #########

# ######### update shipment #########
class RedboxsaUpdateShipmentRequest(BaseModel):
    items: List
    point_id: str
    tracking_number: str  # require if no shipment_id
    shipment_id: str  # require if no tracking_id
    reference: str
    customer_name: str
    customer_phone: str
    customer_email: str
    customer_address: str
    size: str  # Required and one of sizes: "Small", "Medium", "Large"
    dimension_unit: Optional[str]
    dimesion_length: Optional[float]
    dimension_width: Optional[float]
    dimension_height: Optional[float]
    cod_currency: str
    cod_amount: str
    weight_unit: Optional[str]
    weight_value: Optional[float]


class RedboxsaUpdateShipmentResponse(BaseModel):
    success: bool


# ######### update shipment #########

# ######### get list shipments#########
class RedboxsagetListShipmentsRequest(BaseModel):
    page: int


class RedboxsagetListShipmentsResponse(BaseModel):
    success: bool
    data: List
    paging: Dict



# ######### get list shipments #########


# ######### shipment detail #########
class RedboxsaShipmentDetailRequest(BaseModel):
    tracking_number: str  # require if no shipment_id


class RedboxsaShipmentDetailResponse(BaseModel):
    success: bool
    shipment: Dict


# ######### shipment detail #########


# ######### shipment detail #########
class RedboxsaShipmentShippingLabelRequest(BaseModel):
    tracking_number: str  # require if no shipment_id

class RedboxsaShipmentShippingLabelResponse(BaseModel):
    success: bool
    url: str
# ######### shipment detail #########

# ######### cancel shipment #########
class RedboxsaCancelShipmentRequest(BaseModel):
    tracking_number: str  # require if no shipment_id


class RedboxsaCancelShipmentResponse(BaseModel):
    success: bool
# ######### cancel shipment #########


# ######### return shipment #########
class RedboxsaReturnShipmentRequest(BaseModel):
    tracking_number: str  # require if no shipment_id

class RedboxsaReturnShipmentResponse(BaseModel):
    success: bool
    shipment_id: str
    tracking_number: str
# ######### return shipment #########

# ######### return shipment #########
class RedboxsaUpdateShipmentCodRequest(BaseModel):
    shipment_id: str  # require if no tracking_id
    tracking_number: Optional[str]  # require if no shipment_id
    cod_currency: str
    cod_amount: float


class RedboxsaUpdateShipmentCodResponse(BaseModel):
    success: bool
# ######### return shipment #########


# ######### extend shipment #########
class RedboxsaExtendShipmentRequest(BaseModel):
    shipment_id: str  # require if no tracking_id
    tracking_number: Optional[str]  # require if no shipment_id
    day_extend: int


class RedboxsaExtendShipmentResponse(BaseModel):
    success: bool
# ######### extend shipment #########


# ######### Create Request Pickup #########
class RedboxsaCreateRequestPickupRequest(BaseModel):
    business_id: str

class RedboxsaCreateRequestPickupResponse(BaseModel):
    success: bool
# ######### Create Request Pickup #########


# ######### shipment Status #########
class RedboxsaShipmentStatusRequest(BaseModel):
    tracking_number: str

class RedboxsaShipmentStatusResponse(BaseModel):
    success: bool
    status: str

# ######### shipment Status #########

