from os import error
from models import Event, Hello
import helper
import json
import random

helper = helper.Helper()


def handler(e, _):
    try:
        event = Event(**e)
        request_body = Hello(**json.loads(event.body))
        response_body = helper.save_hello(request_body)
        return {
            "statusCode": 201,
            "headers": {
                'Content-Type': 'application/json',
                'Random-Num': random.randint(0,1000),
            },
            "body": json.dumps(response_body.dict()),
            "isBase64Encoded": False
        }
    except Exception as error:
        raise error