#  Copyright (c) 2022. Golden Scent LLC
#  dhanasekar@goldenscent.com
#

from aws_lambda_powertools.utilities.parser import ValidationError
from redboxsa_models import RedboxsaCancelShipmentRequest, RedboxsaCancelShipmentResponse
import redboxsa_helper
import json
import sys
import os


def handler(e, _):
    try:
        request_body = RedboxsaCancelShipmentRequest.parse_obj(
            json.loads(e['body'])
        )
        payload = json.dumps(request_body.dict())
        response = redboxsa_helper.putCancelShipment(payload)

        if response.status_code != 200:
            status_code = response.status_code
            body = {"message": response.text}
        else:
            body = RedboxsaCancelShipmentResponse.parse_obj(
                json.loads(response.text)
            ).dict()
            status_code = response.status_code

    except ValidationError as error:
        status_code = 403
        body = {"message": error.errors()}
    except Exception as error:
        status_code = 403
        body = {"message": error}

    return {
        "statusCode": status_code,
        "headers": {
            'Content-Type': 'application/json'
        },
        "body": json.dumps(body),
        "isBase64Encoded": False
    }
