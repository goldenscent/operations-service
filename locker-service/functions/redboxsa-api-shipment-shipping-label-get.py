#  Copyright (c) 2022. Golden Scent LLC
#  dhanasekar@goldenscent.com
#
#

from aws_lambda_powertools.utilities.parser import ValidationError
from redboxsa_models import RedboxsaShipmentShippingLabelRequest, RedboxsaShipmentShippingLabelResponse
import redboxsa_helper
import json
import sys
import os


def handler(e, _):
    try:
        request_body = RedboxsaShipmentShippingLabelRequest.parse_obj(
            e['pathParameters']
        )
        response = redboxsa_helper.getShipmentShippingLabel(request_body.tracking_number)

        if response.status_code != 200:
            status_code = response.status_code
            body = {"message": response.text}
        else:
            body = RedboxsaShipmentShippingLabelResponse.parse_obj(
                json.loads(response.text)
            ).dict()
            status_code = response.status_code

    except ValidationError as error:
        status_code = 403
        body = {"message": error.errors()}
    except Exception as error:
        status_code = 403
        body = {"message": error}

    return {
        "statusCode": status_code,
        "headers": {
            'Content-Type': 'application/json'
        },
        "body": json.dumps(body),
        "isBase64Encoded": False
    }
