#!/bin/bash
echo 'Deploying Bullet Delivery'
# deploy lambda funtion
sam deploy --config-file=samconfig-prod.toml \
--profile=default \
--config-env=prod \
--no-confirm-changeset \
--no-fail-on-empty-changeset \
DefaultEnv=PROD \
Stage=PROD \
BulletDeliveryBaseUrl="https://live.barqfleet.com/api/v1/merchants" \
VpcsecurityGroup=sg-02a1607f \
Subnet1=subnet-080b0085987fc0da7 \
Subnet2=subnet-00f8927a9f7fb2ab2 \
--debug