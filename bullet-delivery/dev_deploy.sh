#!/bin/bash
echo 'Deploying Bullet Delivery'
# deploy lambda funtion
sam deploy --config-file=samconfig.toml \
--profile=default \
--config-env=dev \
--no-confirm-changeset \
--no-fail-on-empty-changeset \
DefaultEnv=DEV \
VpcsecurityGroup=sg-02a1607f \
Subnet1=subnet-080b0085987fc0da7 \
Subnet2=subnet-00f8927a9f7fb2ab2 \
--debug