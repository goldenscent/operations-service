import json
import sys
import requests
import os
import helper
import logging
import boto3

# Configure logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

sqs_client = boto3.client('sqs')

# Define the status mapping dictionary


status_mapping = {
    "new_order": "delivery_in_progress",
    "processing": "delivery_in_progress",
    "ready_for_delivery": "delivery_in_progress",
    "picked_up": "delivery_ofd",
    "intransit": "delivery_ofd",
    "completed": "delivered",
    "cancelled": "canceled",
    "exception": "delivery_exception",
    "returned": "delivery_returned",
    "damaged_or_lost": "damaged",
    "not_ready_for_pickup": "delivery_on_hold",
    "pickup_failed": "delivery_retrying",
    "processing_at_warehouse": "delivery_in_progress",
    "in_warehouse": "delivery_in_progress",
    "misrouted": "delivery_in_progress",
    "in_transit_to_warehouse": "delivery_in_progress",
    "delivery_attempt_failed": "delivery_retrying"
}


# Define the should_update_status function
def should_update_status(old_status, new_status):
    return (
            (new_status != old_status and old_status != "delivered") or
            (old_status == "delivered" and new_status == 'delivery_pending_return')
    ) and old_status != "closed"


# Define a function to map old status to new status
def map_status(old_status):
    return status_mapping.get(old_status, old_status)  # Return the mapped status or the original if not found


def handler(event, context):
    try:
        stage = os.getenv('STAGE', 'DEV').upper()

        for message in event.get('Records'):
            process_message(message, stage)

        return {
            'statusCode': 200,
            'body': json.dumps({"message": "done"})
        }

    except Exception as e:
        logger.error("Bullet_Delivery Tracking: Exception - %s", str(e))
        return {
            "statusCode": 500,
            "body": json.dumps(str(e))
        }


def process_message(message, stage):
    logger.info("processing message: ")
    logger.info(message.get("body"))
    message_body = json.loads(message.get("body"))
    awb = message_body.get("track_number")
    increment_id = message_body.get("increment_id")

    # Log calling the bullet delivery API
    logger.info("calling bullet delivery API for awb: " + awb)

    order_details = helper.get_order_details(awb, stage)

    # Log the API response
    logger.info("bullet delivery API response: " + json.dumps(order_details))

    if order_details and 'body' in order_details:
        # Parse the JSON string within the 'body' key
        body_content = json.loads(order_details['body'])

        # Check if 'order_status' exists in the parsed body content
        if 'order_status' in body_content:
            tracking_status = body_content['order_status']

            # Map the tracking status to the new status
            tracking_status = map_status(tracking_status)

            # Get the order details from the helper method
            order_list = helper.get_order(stage, increment_id)

            if order_list:
                old_order_status = order_list[0].get('status')  # Correct the key based on returned data

                # Log old and new status
                logger.info("old status: " + old_order_status + ", new status: " + tracking_status)

                if should_update_status(old_order_status, tracking_status):
                    payload = {
                        'body': json.dumps({
                            "orderId": increment_id,
                            "status": tracking_status,
                            "status_description": "Bullet Delivery Tracking API"
                        })
                    }
                    invoke_lambda = boto3.client("lambda", region_name="eu-west-1")
                    invoke_function_name = f"arn:aws:lambda:eu-west-1:439413061158:function:gs-common-update-order-status:{stage}"
                    resp = invoke_lambda.invoke(
                        FunctionName=invoke_function_name,
                        InvocationType="RequestResponse",
                        Payload=json.dumps(payload)
                    )

                    # Log the response from the Lambda function
                    logger.info(json.loads(resp['Payload'].read()))

    # todo: uncomment after testing
    # sqs.delete_message(QueueUrl=os.environ['SQS_QUEUE_URL'], ReceiptHandle=message.get("receiptHandle"))
