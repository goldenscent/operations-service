import json
import requests
import helper
import os
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


secret_name_email = "BulletDeliveryCredentials"
secret_name_password = "BulletDeliveryCredentials"  # Can be the same ARN


def handler(event, context):

    try:
        stage = os.getenv('STAGE', 'DEV').upper()

        bulletDeliverySecrets = helper.get_secret("BULLETDELIVERY/CREDENTIALS", stage)
        logger.info("Bullet_Delivery Login: Retrieved secrets")

        bulletDeliveryCredentials = {"email": bulletDeliverySecrets['email'], "password": bulletDeliverySecrets['password']}
        logger.info("Bullet_Delivery Login: Prepared credentials")

        response = helper.postRequest(json.dumps(bulletDeliveryCredentials), "/login", stage)
        logger.info(f"Bullet_Delivery Login: Received response - {response.status_code} - {response.text}")


        if response.status_code != 200:
            logger.error(f"Bullet_Delivery Login: Error response from API - {response.text}")
            return {
                "statusCode": 500,
                "body": response.text
            }
        response_data = response.json()
        if 'token' in response_data:
            helper.save_secret("BULLETDELIVERY/TOKEN", {"token": response_data['token']}, stage)
            logger.info(f"Bullet_Delivery Token has been updated")


        return {"body": response.text}

    except requests.RequestException as e:
        logger.error("Bullet_Delivery Login: RequestException - %s", str(e))

        return {
            "statusCode": 500,
            "body": json.dumps(str(e))
        }

    except Exception as e:
        logger.error("Bullet_Delivery Login: Exception - %s", str(e))

        return {
            "statusCode": 500,
            "body": json.dumps(str(e))
        }


