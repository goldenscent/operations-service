import json
import requests
from pydantic import ValidationError
from models import Order
import os
import helper
import logging
import sys
import base64

# Configure logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)


def handler(event, context):
    try:
        stage = os.getenv('STAGE', 'DEV').upper()

        # Handle Base64 encoding
        if event.get('isBase64Encoded', False):
            body = base64.b64decode(event['body'])
        else:
            body = event['body']

        body = json.loads(body)

        logger.info(f"Bullet_Delivery Create Order: Received body - {body}")

        try:
            order_data = Order(**body)
            logger.info("Bullet_Delivery Create Order: Order data validated successfully")
        except ValidationError as e:
            logger.error("Bullet_Delivery Create Order: Validation error - %s", e.json())
            return {
                "statusCode": 400,
                "body": e.json()
            }

        response = helper.postRequest(order_data.json(), "/orders", stage)
        logger.info(f"Bullet_Delivery Create Order: Received response - {response.status_code} - {response.text}")

        if response.status_code != 201:
            logger.error(f"Bullet_Delivery Create Order: Error response from API - {response.text}")
            return {
                "statusCode": 500,
                "body": response.text
            }

        return {
            "statusCode": 201,
            "body": response.text
        }

    except requests.RequestException as e:
        logger.error("Bullet_Delivery Create Order: RequestException - %s", str(e))
        return {
            "statusCode": 500,
            "body": json.dumps(str(e))
        }

    except Exception as e:
        logger.error("Bullet_Delivery Create Order: Exception - %s", str(e))
        return {
            "statusCode": 500,
            "body": json.dumps(str(e))
        }