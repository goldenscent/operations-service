import json
import os
import logging
import boto3
import helper


logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s')
formatter.default_msec_format = '%s.%03d'

status_mapping = {
    "new_order": "delivery_in_progress",
    "processing": "delivery_in_progress",
    "ready_for_delivery": "delivery_in_progress",
    "picked_up": "delivery_ofd",
    "intransit": "delivery_ofd",
    "completed": "delivered",
    "cancelled": "canceled",
    "exception": "delivery_exception",
    "returned": "delivery_returned",
    "damaged_or_lost": "damaged",
    "not_ready_for_pickup": "delivery_on_hold",
    "pickup_failed": "delivery_retrying",
    "processing_at_warehouse": "delivery_in_progress",
    "in_warehouse": "delivery_in_progress",
    "misrouted": "delivery_in_progress",
    "in_transit_to_warehouse": "delivery_in_progress",
    "delivery_attempt_failed": "delivery_retrying"
}

lambda_client = boto3.client('lambda')


def get_web_tracking(awb, stage):
    payload = {'body': '{"awb":"' + awb + '"}'}
    result = lambda_client.invoke(
        FunctionName="gs-get-webtracking-link:" + stage,
        InvocationType='RequestResponse',
        Payload=json.dumps(payload)
    )
    if result.get('StatusCode') == 200:
        return json.loads(json.loads(result.get('Payload').read()).get('body'))
    return None




def handler(event, context):

    stage = os.getenv('STAGE', 'DEV').upper()

    response = {}
    status_code = 200
    if 'pathParameters' in event and 'awb' in event['pathParameters']:

        logger.info("Bullet_Delivery tracking %s" % event)
        awbs = event['pathParameters']['awb'].split(',')

        web_tracking_link = get_web_tracking(event['pathParameters']['awb'], stage)
        logger.info("web_tracking_link:%s" %  json.dumps(web_tracking_link))

        for awb in awbs:
            try:
                logger.info("Bullet_Delivery tracking awb: %s" % awb)
                order_details = helper.get_order_details(awb, stage)

                order_details = json.loads(order_details['body'])
                logger.info("Bullet_Delivery body_data: %s" % json.dumps(order_details))

                if order_details:
                    if 'order_status' in order_details:
                        response[awb] = {'updates': order_details.get("order_status")}
                        response[awb]['status'] = status_mapping.get(order_details.get("order_status"), None)
                        logger.info("Bullet_Delivery tracking response before link: %s" % json.dumps(response))


                        logger.info("Bullet_Delivery tracking link:" + str(web_tracking_link))

                        if web_tracking_link and awb in web_tracking_link:
                            response[awb]['tracking_url'] = web_tracking_link[awb]

            except Exception as e:
                logger.error(e)

    return {
        'statusCode': status_code,
        'body': json.dumps(response)
    }