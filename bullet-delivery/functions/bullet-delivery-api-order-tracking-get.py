import json
import requests
from pydantic import ValidationError
from models import GetOrder
import os
import helper
import logging

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def handler(event, context):
    try:
        stage = os.getenv('STAGE', 'DEV').upper()

        pathParameters = GetOrder(**event['pathParameters'])
        logger.info(f"Bullet_Delivery Tracking: Query parameters - {pathParameters}")

        orderId = pathParameters.order_id
        logger.info(f"Bullet_Delivery Get Order: Order ID - {orderId}")

        response = helper.getRequest("/orders/" + str(orderId) + "/tracking", stage)
        logger.info(f"Bullet_Delivery Tracking: Received response - {response.status_code} - {response.text}")

        if response.status_code != 200:
            logger.error(f"Bullet_Delivery Tracking: Error response from API - {response.text}")
            return {
                "statusCode": 500,
                "body": response.text
            }

        return {
            "statusCode": 200,
            "body": response.text
        }

    except ValidationError as e:
        logger.error("Bullet_Delivery Tracking: Validation error - %s", e.errors())
        return {
            "statusCode": 400,
            "body": json.dumps({"error": e.errors()})
        }

    except requests.RequestException as e:
        logger.error("Bullet_Delivery Tracking: RequestException - %s", str(e))
        return {
            "statusCode": 500,
            "body": json.dumps(str(e))
        }

    except Exception as e:
        logger.error("Bullet_Delivery Tracking: Exception - %s", str(e))
        return {
            "statusCode": 500,
            "body": json.dumps(str(e))
        }
