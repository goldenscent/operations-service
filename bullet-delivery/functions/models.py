# models.py
from typing import Optional
from pydantic import BaseModel, conint, confloat, constr, conlist

class CustomerDetails(BaseModel):
    first_name: str
    last_name:  str
    country:    str
    city:       str
    mobile:     constr(regex=r'^\d+$')
    address:    str
    street:     str

class Product(BaseModel):
    sku:       str
    serial_no: str
    name:      str
    color:     str
    brand:     str
    price:     confloat(ge=0)
    weight_kg: confloat(ge=0)
    qty:       conint(gt=0)

class Destination(BaseModel):
    latitude:  confloat(ge=-90, le=90)
    longitude: confloat(ge=-180, le=180)

class Order(BaseModel):
    payment_type:       conint(ge=0)
    shipment_type:      conint(ge=0)
    hub_id:             conint(ge=0)
    hub_code:           str
    merchant_order_id:  str
    invoice_total:      constr(regex=r'^\d+(\.\d{1,2})?$')
    customer_details:   CustomerDetails
    products:           conlist(Product, min_items=1)
    destination:        Optional[Destination] = None


class GetOrder(BaseModel):
    order_id: int
