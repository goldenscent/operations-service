import os
import requests
import logging
import json
import boto3
from datetime import datetime, timedelta
from botocore.exceptions import ClientError
from goldenscentpy.init import init
import goldenscentpy.mysql.connection as gsmysqlcon
import pymysql
import base64

lambda_client = boto3.client('lambda')

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

session = boto3.session.Session()
client = session.client(service_name='secretsmanager', region_name='eu-west-1')


class DatabaseManager:
    def __init__(self, mode="WRITE"):
        self.mode = mode
        self.connection = gsmysqlcon.Connection()  # Assuming this uses pymysql

    def get_connection(self, stage, proxy=False):
        stage = stage.upper()
        if stage == 'PROD':
            proxy = True
        return self.connection.get_connection(stage, mode=self.mode, proxy=proxy)

    def execute_query(self, stage, sql_query, params=None):
        conn = self.get_connection(stage)
        try:
            with conn.cursor(pymysql.cursors.DictCursor) as cursor:  # Use DictCursor here
                cursor.execute(sql_query, params)
                result = cursor.fetchall()
                return result
        except Exception as e:
            logger.error(f"Error during execute_query: {str(e)}")
            raise e

    def commit(self, connection):
        try:
            connection.commit()
        except Exception as e:
            logger.error(f"Error during commit: {str(e)}")
            raise e


# Initialize database manager
db_manager = DatabaseManager()


def databaseTest(stage, db_manager=db_manager):
    try:
        sql_query = "SELECT count(*) AS table_count FROM information_schema.TABLES;"
        result = db_manager.execute_query(stage, sql_query)
        logger.info(f"DB Connection Test Successful: {result}")
        return result
    except Exception as e:
        logger.error(f"DB Connection Test Failed: {str(e)}")
        raise e


def save_secret(secret_name, secret_value, stage):
    try:
        full_secret_name = f"/{stage}/{secret_name}"
        client.put_secret_value(
            SecretId=full_secret_name,
            SecretString=json.dumps(secret_value)
        )
        logger.info(f"Bullet_Delivery Login: Successfully saved token to {full_secret_name}")
    except ClientError as e:
        logger.error(f"Bullet_Delivery Login: Failed to save token - {str(e)}")
        raise e


def get_secret(secret_name, stage):
    try:
        full_secret_name = f"/{stage}/{secret_name}"
        logger.info(f"Bullet_Delivery Get Secret: Retrieving secret '{full_secret_name}'")

        get_secret_value_response = client.get_secret_value(SecretId=full_secret_name)

        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
            logger.info("Bullet_Delivery Get Secret: Secret string retrieved successfully")
            return json.loads(secret)
        else:
            secret = get_secret_value_response['SecretBinary']
            logger.info("Bullet_Delivery Get Secret: Secret binary retrieved successfully")
            return json.loads(secret)

    except ClientError as e:
        logger.error("Bullet_Delivery Get Secret: Error occurred while retrieving secret - %s", str(e))
        raise e


def postRequest(payload, url, stage):
    try:
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        secret = get_secret('BULLETDELIVERY/TOKEN', stage)
        headers['Authorization'] = secret.get('token')

        full_url = f"{os.environ['BULLET_DELIVERY_BASE_URL']}{url}"
        logger.info(f"Bullet_Delivery Post Request: Sending POST request to '{full_url}'")

        if not payload:
            response = requests.request(
                "POST",
                full_url,
                headers=headers,
                verify=False
            )
        else:
            response = requests.request(
                "POST",
                full_url,
                headers=headers,
                json=json.loads(payload),
                verify=False
            )

        response.raise_for_status()
        logger.info("Bullet_Delivery Post Request: POST request successful")
        return response

    except requests.RequestException as e:
        logger.error(f"Bullet_Delivery Post Request: Request failed - {e}")
        raise e

    except Exception as e:
        logger.error(f"Bullet_Delivery Post Request: An unexpected error occurred - {e}")
        raise e


def getRequest(url, stage):
    try:
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        # Check if URL includes "airwaybill" and adjust headers if necessary
        if 'airwaybill' in url.lower():
            headers['Accept'] = 'application/pdf'

        secret = get_secret('BULLETDELIVERY/TOKEN', stage)
        headers['Authorization'] = secret.get('token')

        full_url = f"{os.environ['BULLET_DELIVERY_BASE_URL']}{url}"
        logger.info(f"Bullet_Delivery Get Request: Sending GET request to '{full_url}'")

        response = requests.request(
            "GET",
            full_url,
            headers=headers,
            verify=False
        )

        response.raise_for_status()
        logger.info("Bullet_Delivery Get Request: GET request successful")

        content_type = response.headers.get('Content-Type', '')
        logger.info(content_type)

        if 'application/pdf' in content_type:
            logger.info("Bullet_Delivery Get Request: Received PDF response")
            return {
                'statusCode': 200,
                'headers': {
                    'Content-Type': 'application/pdf',
                },
                #'body': response.content,  # Return raw binary content
                'body': base64.b64encode(response.content).decode('utf-8'),  # Return base64 encoded PDF content
                'isBase64Encoded': True
            }
        else:
            logger.info("Bullet_Delivery Get Request: Received JSON response")
            return response


    except requests.RequestException as e:
        logger.error(f"Bullet_Delivery Get Request: Request failed - {e}")
        raise e

    except Exception as e:
        logger.error(f"Bullet_Delivery Get Request: An unexpected error occurred - {e}")
        raise e

def get_order_details(awb, stage):
    stage = stage.lower()

    payload = {"pathParameters": {"order_id": awb}}
    result = lambda_client.invoke(
        FunctionName=f"bullet-delivery-api-get-orders-details-get-{stage}",
        InvocationType='RequestResponse',
        Payload=json.dumps(payload)
    )
    if result.get('StatusCode') == 200:
        return json.loads(result.get('Payload').read())
    return None

# Push SQS
def get_orders_to_track(stage, carrier_code, order_status, days, db_manager=db_manager):

    try:
        logger.info(f"Bullet_Delivery Queue Push: get_orders_to_track - {stage}, {carrier_code}, {order_status}, {days}")

        # Use placeholders in the SQL query
        sql_query = f"""
            SELECT o.entity_id, o.increment_id, carrier_code, o.status, JSON_UNQUOTE(JSON_EXTRACT(s.additional_info, '$.barq_order_id')) AS track_number
            FROM sales_flat_order o
            INNER JOIN sales_flat_shipment_track s ON s.order_id = o.entity_id
            WHERE s.carrier_code = %s AND o.status IN ({order_status}) AND
                  o.updated_at > DATE_ADD(NOW(), INTERVAL - %s DAY)
            ORDER BY o.created_at
        """

        # Execute the query with parameters
        result = db_manager.execute_query(stage, sql_query, params=(carrier_code, days))

        logger.info("Fetched orders to track successfully")
        return result

    except Exception as e:
        logger.error(f"Error fetching orders to track: {str(e)}")
        raise e


# Refresh queue
def get_order(stage, increment_id, db_manager=db_manager):
    try:
        sql_query = "SELECT entity_id, status, state, store_id, increment_id FROM sales_flat_order WHERE increment_id = %s"

        result = db_manager.execute_query(stage, sql_query, params=(increment_id))
        logger.info(f"Fetched order with increment_id {increment_id} successfully")
        return result

    except Exception as e:
        logger.error(f"Error fetching order with increment_id {increment_id}: {str(e)}")
        raise e


def update_order_status(stage, increment_id, order_status, db_manager=db_manager):
    try:
        sql_query = "UPDATE sales_flat_order SET updated_at=NOW(), status = %s WHERE increment_id = %s"
        sql_query2 = "UPDATE sales_flat_order_grid SET updated_at=NOW(), status = %s WHERE increment_id = %s"
        sql_query3 = "UPDATE enterprise_sales_order_grid_archive SET updated_at=NOW(), status = %s WHERE increment_id = %s"
        condition = (order_status, increment_id)

        db_manager.execute_query(stage, sql_query, params=condition)
        db_manager.execute_query(stage, sql_query2, params=condition)
        db_manager.execute_query(stage, sql_query3, params=condition)

        logger.info(f"Updated order status for increment_id {increment_id} to {order_status} successfully")

    except Exception as e:
        logger.error(f"Error updating order status for increment_id {increment_id}: {str(e)}")
        raise e


def add_status_history_comment(stage, entity_id, is_customer_notified, is_visible_on_front, comment, status, db_manager=db_manager):
    try:
        now = datetime.now()
        formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')

        sql_query = """INSERT INTO sales_flat_order_status_history 
                       (parent_id, is_customer_notified, is_visible_on_front, comment, status, created_at, entity_name) 
                       VALUES (%s, %s, %s, %s, %s, %s, %s)"""
        condition = (entity_id, is_customer_notified, is_visible_on_front, comment, status, formatted_date, 'order')

        db_manager.execute_query(stage, sql_query, params=condition)
        logger.info(f"Added status history comment for entity_id {entity_id} successfully")

    except Exception as e:
        logger.error(f"Error adding status history comment for entity_id {entity_id}: {str(e)}")
        raise e



