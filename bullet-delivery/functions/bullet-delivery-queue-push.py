import json
import sys
import os
import helper
import logging
import boto3

# Configure logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)


order_status = json.loads(os.environ['ORDER_STATUS'])
days = os.environ['DAYS_TO_SYNC']
sqs_client = boto3.client('sqs')
bullet_delivery_queue_url = os.getenv("SQS_QUEUE_URL")


def handler(event, context):
    try:
        stage = os.getenv('STAGE', 'DEV').upper()

        order_status_concat = ', '.join("'{0}'".format(w) for w in order_status)
        carrier_code = os.environ['CARRIER_CODE']
        logger.info(f"Bullet_Delivery Queue Push: get_orders_to_track - {stage}, {carrier_code}, {order_status_concat}, {days}")

        orders = helper.get_orders_to_track(stage, carrier_code, order_status_concat, days)

        for order in orders:
            sqs_message = json.dumps(order)
            logger.info(f"Bullet_Delivery Queue Push: Order ID - {order.get('increment_id')}" + " - " + sqs_message)
            sqs_client.send_message(QueueUrl=bullet_delivery_queue_url, MessageBody=sqs_message)

        return {
            'statusCode': 200,
            'body': json.dumps({"count": len(orders), "messages": orders})
        }

    except Exception as e:
        logger.error("Bullet_Delivery Tracking: Exception - %s", str(e))
        return {
            "statusCode": 500,
            "body": json.dumps(str(e))
        }

