import json
import os
import logging
import helper


def handler(event, context):
    try:
        stage = os.getenv('STAGE', 'DEV').upper()

        result = helper.databaseTest(stage)
        print(result)

        return {
            "statusCode": 200,
            "body": json.dumps({"message": "Database connection successful", "result": result})
        }

    except Exception as e:
        print("Bullet_Delivery Tracking: Exception - %s", str(e))
        return {
            "statusCode": 500,
            "body": json.dumps(str(e))
        }
