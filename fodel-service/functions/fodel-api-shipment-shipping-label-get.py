#  Copyright (c) 2022-2023. Golden Scent LLC
#  dhanasekar@goldenscent.com
#  Dhanasekar P

from aws_lambda_powertools.utilities.parser import ValidationError
from fodel_models import FodelShipmentShippingLabelRequest, FodelShipmentShippingLabelResponse
import fodel_helper
import json
import sys
import os


def handler(e, _):
    try:
        request_body = FodelShipmentShippingLabelRequest.parse_obj(
            e['pathParameters']
        )
        response = fodel_helper.getShipmentShippingLabel(request_body.tracking_number)

        if response.status_code != 200:
            status_code = response.status_code
            body = {"message": response.text}
        else:
            body = FodelShipmentShippingLabelResponse.parse_obj(
                json.loads(response.text)
            ).dict()
            status_code = response.status_code

    except ValidationError as error:
        status_code = 403
        body = {"message": error.errors()}
    except Exception as error:
        status_code = 403
        body = {"message": error}

    return {
        "statusCode": status_code,
        "headers": {
            'Content-Type': 'application/json'
        },
        "body": json.dumps(body),
        "isBase64Encoded": False
    }
