#  Copyright (c) 2023. Golden Scent LLC
#  dhanasekar@goldenscent.com
#

from typing import Dict, List, Optional, Literal
from pydantic import BaseModel, root_validator, parse_obj_as, ValidationError, validator
from datetime import datetime
from enum import Enum, IntEnum


class Event(BaseModel):
    resource: str
    body: Optional[Dict]
    queryStringParameters: Optional[Dict[str, str]]
    multiValueQueryStringParameters: Optional[Dict[str, List]]


# ######### points request #########
class FodelPointsRequest(BaseModel):
    lat: float
    lon: float
    appKey: str
    lang: str
    country: str


class FodelPointsResponse(BaseModel):
    data: Dict


# ######### points request #########

# ######### create shipment #########
class FodelCreateShipmentRequest(BaseModel):
    product_info: List
    reference_id: str
    order_no: str
    collection_point_id: str
    customer_name: str
    customer_phone: str
    customer_address: str
    is_cod: str
    cod_currency: str
    cod_amount: str
    lang: str
    weight: float
    volume_length: float
    volume_width: float
    volume_width: float


class FodelCreateShipmentResponse(BaseModel):
    data: Dict

# ######### create shipment #########


# ######### shipment label #########

class FodelShipmentShippingLabelRequest(BaseModel):
    tracking_number: str

class FodelShipmentShippingLabelResponse(BaseModel):
    data: Dict

# ######### shipment label #########

# ######### shipment cancel #########

class FodelCancelShipmentRequest(BaseModel):
    tracking_number: str

class FodelCancelShipmentResponse(BaseModel):
    msg: str
    data: Dict

# ######### shipment cancel #########


# ######### shipment detail #########
class FodelShipmentDetailRequest(BaseModel):
    tracking_number: str  # require if no shipment_id


class FodelShipmentDetailResponse(BaseModel):
    data: Dict

# ######### shipment detail #########


# ######### shipment Status #########
class FodelShipmentStatusRequest(BaseModel):
    awbs: str

class FodelShipmentStatusResponse(BaseModel):
    data: List

# ######### shipment Status #########
