#  Copyright (c) 2023. Golden Scent LLC
#  dhanasekar@goldenscent.com
#  Dhanasekar P

from aws_lambda_powertools.utilities.parser import ValidationError
from fodel_models import FodelCreateShipmentRequest, FodelCreateShipmentResponse
import fodel_helper
import json
import sys
import os


def handler(e, _):
    try:
        request_body = FodelCreateShipmentRequest.parse_obj(
            json.loads(e['body'])
        )
        payload = json.dumps(request_body.dict())
        response = fodel_helper.postCreateShipment(payload)

        if response.status_code != 200:
            status_code = response.status_code
            body = {"message": response.text}
        else:
            body = FodelCreateShipmentResponse.parse_obj(
                json.loads(response.text)
            ).dict()
            status_code = response.status_code

    except ValidationError as error:
        status_code = 403
        body = {"message": error.errors()}
    except Exception as error:
        status_code = 403
        body = {"message": error}

    return {
        "statusCode": status_code,
        "headers": {
            'Content-Type': 'application/json'
        },
        "body": json.dumps(body),
        "isBase64Encoded": False
    }
