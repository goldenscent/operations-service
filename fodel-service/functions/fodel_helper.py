#  Copyright (c) 2023. Golden Scent LLC
#  dhanasekar@goldenscent.com
#

import os
import requests
import boto3
import sys


def getPoints(lat, lon, appKey, lang, country):
    headers = {
        'Authorization': os.environ['FODEL_API_KEY'],
        'Content-Type': 'application/json',
    }
    response = requests.request(
        "GET",
        f"{os.environ['FODEL_BASE_URL']}collectionpoint/list?lat={lat}&lon={lon}&appKey={appKey}&lang={lang}&country={country}",
        headers=headers,
    )
    return response


def postCreateShipment(payload):
    headers = {
        'Authorization': os.environ['FODEL_API_KEY'],
        'Content-Type': 'application/json',
    }
    response = requests.request(
        "POST",
        f"{os.environ['FODEL_BASE_URL']}shipments",
        headers=headers,
        data=payload
    )
    return response

def getShipmentShippingLabel(tracking_number):
    headers = {
        'Authorization': os.environ['FODEL_API_KEY'],
        'Content-Type': 'application/json',
    }

    response = requests.request(
        "GET",
        f"{os.environ['FODEL_BASE_URL']}label/url/pdf/{tracking_number}",
        headers=headers,
    )
    return response

def deleteCancelShipment(tracking_number):
    headers = {
        'Authorization': os.environ['FODEL_API_KEY'],
        'Content-Type': 'application/json',
    }
    response = requests.request(
        "DELETE",
        f"{os.environ['FODEL_BASE_URL']}shipments/{tracking_number}",
        headers=headers,
    )
    return response

def getShipmentDetail(tracking_number):
    headers = {
        'Authorization': os.environ['FODEL_API_KEY'],
        'Content-Type': 'application/json',
    }
    response = requests.request(
        "GET",
        f"{os.environ['FODEL_BASE_URL']}shipments/{tracking_number}",
        headers=headers,
    )
    return response

def getShipmentStatus(tracking_number):
    headers = {
        'Authorization': os.environ['FODEL_API_KEY'],
        'Content-Type': 'application/json',
    }
    response = requests.request(
        "GET",
        f"{os.environ['FODEL_BASE_URL']}shipments?awbs={tracking_number}",
        headers=headers,
    )
    return response