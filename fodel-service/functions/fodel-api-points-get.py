#  Copyright (c) 2023. Golden Scent LLC
#  dhanasekar@goldenscent.com
#  Dhanasekar P

from aws_lambda_powertools.utilities.parser import ValidationError
from fodel_models import FodelPointsRequest, FodelPointsResponse
import fodel_helper
import json
import sys
import os


def handler(e, _):
    try:
        request_body = FodelPointsRequest.parse_obj(
            e['pathParameters']
        )

        response = fodel_helper.getPoints(request_body.lat, request_body.lon, request_body.appKey, request_body.lang, request_body.country)
        if response.status_code != 200:
            status_code = response.status_code
            body = {"message": response.text}
        else:
            body = FodelPointsResponse.parse_obj(
                json.loads(response.text)
            ).dict()
            status_code = response.status_code

    except ValidationError as error:
        status_code = 401
        body = {"message": error.errors()}
    except Exception as error:
        status_code = 403
        body = {"message": error}

    return {
        "statusCode": status_code,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        "body": json.dumps(body),
        "isBase64Encoded": False
    }
