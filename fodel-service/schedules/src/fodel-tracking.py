#  Copyright (c) 2023. Golden Scent LLC
#  dhanasekar@goldenscent.com
#

import json
from init import *
import os
import ssm
import requests
import logging
import boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s')
formatter.default_msec_format = '%s.%03d'

param_stores = {}
status_mapping = {
    "order_placed":"fodel_processing",
    "in_transit_to_collection_point": "fodel_processing",
    "handed_over_to_collection_point": "delivery_in_progress",
    "collected_by_customer": "delivered",
    "Expired": "delivery_in_progress",
    "rto_in_transit_to_client": "delivery_pending_return",
    "rto_delivered_to_client": "delivery_returned",
    "order_cancelled":"canceled",
    "unknown":"lost",
    "lost":"lost",
    "damage":"damage"
}

lambda_client = boto3.client('lambda')


def get_web_tracking(awb, env):
    payload = {'body': '{"awb":"' + awb + '"}'};
    result = lambda_client.invoke(FunctionName="gs-get-webtracking-link:" + env, InvocationType='RequestResponse',
                                  Payload=json.dumps(payload))
    if result.get('StatusCode') == 200:
        return json.loads(json.loads(result.get('Payload').read()).get('body'))
    return None


@init
def handler(event, context, env):
    global param_stores

    # use cached common parameter store object if available
    if not env in param_stores:
        param_stores[env] = ssm.ParamStore(env)

    param_store = param_stores[env]

    response = {}
    status_code = 200
    if 'pathParameters' in event and 'awbs' in event['pathParameters']:
        logger.info("fodel tracking %s" % (event))
        awbs = event['pathParameters']['awbs'].split(',')
        api_url = param_store.get_parameter('FODEL/TRACKING_API_URL', True)

        web_tracking_link = get_web_tracking(event['pathParameters']['awbs'], env)
        # get awb status
        for awb in awbs:
            try:
                logger.info("fodel tracking awb: %s" % (awb))
                logger.info(api_url + awb)
                fodel_response = requests.get(api_url + awb)
                logger.info("fodel tracking response: %s %s" % (fodel_response.status_code, fodel_response.text))
                if fodel_response.status_code == 200:
                    tracking_response = json.loads(fodel_response.text)
                    tracking = tracking_response.get('data')[0]
                    if 'status' in tracking:
                        response[awb] = {'updates': tracking.get("status")}
                        response[awb]['status'] = status_mapping[tracking.get("status")] if tracking.get("status") in status_mapping else None
                        if web_tracking_link and web_tracking_link.get(awb):
                            response[awb]['tracking_url'] = web_tracking_link.get(awb)

            except Exception as e:
                logger.error(e)

    return {
        'statusCode': status_code,
        'body': json.dumps(response)
    }