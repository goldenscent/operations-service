#  Copyright (c) 2023. Golden Scent LLC
#  dhanasekar@goldenscent.com
#

import ssm
import pymysql
import logging
import sys
from datetime import datetime


class Database(object):
    def __init__(self, env):
        self.env = env
        param_store = ssm.ParamStore(env)
        self.db_host = param_store.get_parameter('DB/PROXY')
        self.db_port = param_store.get_parameter('DB/PORT')
        self.db_user = param_store.get_parameter('DB/USER')
        self.db_password = param_store.get_parameter('DB/PASS', True)
        self.db_name = param_store.get_parameter('DB/GS/NAME')  # goldenscent magento DB name
        self.ms_db_name = param_store.get_parameter('DB/MS/NAME')  # stockbroker DB name

        logger = logging.getLogger()
        logger.setLevel(logging.INFO)

        try:
            self.connection = pymysql.connect(self.db_host, user=self.db_user, passwd=self.db_password, db=self.db_name,
                                              connect_timeout=5)
        except:
            logger.error("ERROR: Unexpected error: Could not connect to MySQL instance.")
            sys.exit()

        logger.info("SUCCESS: Connection to RDS MySQL instance succeeded")

    def get_order(self, increment_id):
        with self.connection:
            cur = self.connection.cursor(pymysql.cursors.DictCursor)
            sql_query = "SELECT entity_id, status, state, store_id, increment_id FROM sales_flat_order WHERE increment_id = %s"
            condition = (increment_id,)
            cur.execute(sql_query, condition)
            record = cur.fetchone()
            return record

    def update_order_status(self, increment_id, order_status):
        with self.connection:
            cur = self.connection.cursor()
            sql_query = "UPDATE sales_flat_order SET updated_at=NOW(), status = %s WHERE increment_id = %s"
            sql_query2 = "UPDATE sales_flat_order_grid SET updated_at=NOW(), status = %s WHERE increment_id = %s"
            sql_query3 = "UPDATE enterprise_sales_order_grid_archive SET updated_at=NOW(), status = %s WHERE increment_id = %s"
            condition = (order_status, increment_id)
            cur.execute(sql_query, condition)
            cur.execute(sql_query2, condition)
            cur.execute(sql_query3, condition)

    def add_status_history_comment(self, entity_id, is_customer_notified, is_visible_on_front, comment, status):
        with self.connection:
            now = datetime.now()
            formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')
            cur = self.connection.cursor()
            sql_query = "INSERT INTO sales_flat_order_status_history " \
                        "(parent_id, is_customer_notified, is_visible_on_front, comment, status, created_at, entity_name) " \
                        " VALUES (%s, %s, %s, %s, %s, %s, %s) "
            condition = (entity_id, is_customer_notified, is_visible_on_front, comment, status, formatted_date, 'order')
            cur.execute(sql_query, condition)