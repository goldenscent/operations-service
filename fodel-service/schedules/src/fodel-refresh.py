#  Copyright (c) 2023. Golden Scent LLC
#  dhanasekar@goldenscent.com
#

import json
import boto3
from init import *
import db
import os
import logging
import ssm
from datetime import datetime

import requests

logger = logging.getLogger()
logger.setLevel(logging.INFO)
models = {}
param_stores = {}
fodel_tracking_url = None
sqs = boto3.client('sqs')


@init
def handler(event, context, env):
    global models
    global param_stores
    global fodel_tracking_url

    # use cached common db object if available
    if not env in models:
        models[env] = db.Database(env)

    model = models[env]

    # use cached common parameter store object if available
    if not env in param_stores:
        param_stores[env] = ssm.ParamStore(env)

    param_store = param_stores[env]

    fodel_tracking_url = os.environ["TRACKING_PARENT_API_URL"]

    for message in event.get('Records'):
        process_message(message, model, param_store, env)

    model.connection.commit()

    return {
        'statusCode': 200,
        'body': json.dumps({"message": "done"})
    }


def process_message(message, model, param_store, env):
    logger.info("processing message: " + message.get("body"))
    messageBody = json.loads(message.get("body"))
    awb = messageBody.get("track_number")
    increment_id = messageBody.get("increment_id")
    entity_id = messageBody.get("entity_id")

    # TODO bulk awb check in 1 API call
    logger.info("calling fodel API for awb: " + awb)
    tracking_url = param_store.get_parameter('TRACKING_API_URL')
    courier = ["lambdashipping_fodellocker/","saee/", "aramex/", "smsa/", "barq/"]
    for courier_code in courier:
     response = requests.get(tracking_url + courier_code  + awb)
     if response and len(json.loads(response.text))>=1:
        break
    logger.info("fodel API response: " + response.text)
    if response and response.text:
        tracking_data = json.loads(response.text)
        print(tracking_data)
        if awb in tracking_data and 'status' in tracking_data[awb]:
            tracking_status = tracking_data[awb]['status']
            order = model.get_order(increment_id)
            old_order_status = order.get('status')
            logger.info("old status: " + old_order_status + ", new_status:" + tracking_status)
            if (tracking_status != old_order_status):
                payload = {'body': json.dumps({"orderId":increment_id, "status": tracking_status, "status_description":"Fodel Tracking API"})}
                invoke_lambda = boto3.client("lambda", region_name="eu-west-1")
                invoke_function_name = "arn:aws:lambda:eu-west-1:439413061158:function:gs-common-update-order-status:" + env
                resp = invoke_lambda.invoke(FunctionName=invoke_function_name, InvocationType="RequestResponse",
                                            Payload=json.dumps(payload))

                msgres = json.loads(resp['Payload'].read())
                logger.info(msgres)

    # sqs.delete_message(QueueUrl=os.environ['SQS_QUEUE_URL'], ReceiptHandle=message.get("receiptHandle"))