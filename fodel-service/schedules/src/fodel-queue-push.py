import os
import logging
import boto3
import json
from goldenscentpy.init import init
import goldenscentpy.mysql.connection as gsmysqlcon

logger = logging.getLogger()
logger.setLevel(logging.INFO)
order_status = json.loads(os.environ['ORDER_STATUS'])
days = os.environ['DAYS_TO_SYNC']
sqs_client = boto3.client('sqs')
fodel_refresh_queue_url = os.getenv("QUEUE_URL")
gs_connection_object = gsmysqlcon.Connection()


@init
def handler(event, context, env):
    """Send the orders to fodel refresh track orders

    Parameters
    ----------
    event : object
        event which trigger this function
    context : object
        runtime information
    """
    goldenscent_db_read = gs_connection_object.get_connection(env)
    try:
        order_status_concat = ', '.join(
            "'{0}'".format(w) for w in order_status)
        orders = get_orders_to_track(env, order_status_concat, days)
        for order in orders:
            sqs_message = json.dumps(order)
            logger.info("sending a message to fodel queue: " + sqs_message)
            sqs_client.send_message(
                QueueUrl=fodel_refresh_queue_url, MessageBody=sqs_message)

        goldenscent_db_read.commit()

        return {
            'statusCode': 200,
            'body': json.dumps({"count": len(orders), "messages": orders})
        }

    except Exception as e:
        raise Exception(e)


def get_orders_to_track(env, order_status, days):
    """Send orders to fodel refresh track orders

    Parameters
    ----------
    order_status : str
        status of orders
    days : string
        number of days
    """
    result = []
    sql_query = """SELECT o.entity_id, o.increment_id, s.track_number, carrier_code from sales_flat_order o
                    INNER JOIN sales_flat_shipment_track s ON s.order_id = o.entity_id
                    WHERE s.carrier_code = 'lambdashipping_fodellocker' AND o.status IN (""" + order_status + """)
                    AND o.updated_at > DATE_ADD(NOW(), INTERVAL - """ + days + """ DAY)  ORDER BY o.created_at"""
    cursor = gs_connection_object.execute(env, sql_query, dict_cursor=True)
    result = cursor.fetchall()
    print(cursor._last_executed)
    return result
