from functions import api_hello_get as hello_get
from functions.models import Hello
import json

def test_api_hello_get_handler(api_hello_get_event, lambda_context):
    response = hello_get.handler(api_hello_get_event, lambda_context)
    body = response.get('body')
    hello = Hello(**json.loads(body))
    hello.last_name
    assert hello.first_name == 'mAm'