#!/bin/bash
#
# Copyright (c) 2023. Golden Scent LLC
# dhanasekar@goldenscent.com
#
#

sudo rm -rf layers/python3.8/packages
docker run -v "$PWD/layers/python3.8":/var/task \
"lambci/lambda:build-python3.8" \
/bin/sh -c "pip3 install -r requirements.txt -t packages/python/lib/python3.8/site-packages/; exit"
